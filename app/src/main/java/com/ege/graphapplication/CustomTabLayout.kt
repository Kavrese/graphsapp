package com.ege.graphapplication

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.custom_tab.view.*


class CustomTabLayout(private val tabLayout: TabLayout){

    fun dotsToViewPager2(pager: ViewPager2){
        tabLayout.setSelectedTabIndicator(null)
        tabLayout.tabRippleColor = null
        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab!!.customView!!.dot_img.setImageResource(R.drawable.shape_tab_selected)
                setSquareSize(10.0f, tab.customView!!.dot_img)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab!!.customView!!.dot_img.setImageResource(R.drawable.shape_tab)
                setSquareSize(8.0f, tab.customView!!.dot_img)
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

        })
        TabLayoutMediator(tabLayout, pager){ tab, _ ->
            val view = LayoutInflater.from(tabLayout.context).inflate(R.layout.custom_tab, null)
            setSquareSize(8.0f, view.dot_img)
            tab.customView = view
        }.attach()
    }

    fun TabLayout.getTabs(): List<TabLayout.Tab>{
        val tabs = mutableListOf<TabLayout.Tab>()
        for (i in 0..this.tabCount) {
            tabs.add(this.getTabAt(i)!!)
        }
        return tabs
    }

    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    fun setSquareSize(size: Float, view: View){
        val sizePx = convertDpToPixel(size, tabLayout.context).toInt()
        val layoutParams = ViewGroup.LayoutParams(sizePx, sizePx)
        view.layoutParams = layoutParams
    }
}