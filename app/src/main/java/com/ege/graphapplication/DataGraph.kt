package com.ege.graphapplication

import android.graphics.Color
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.jjoe64.graphview.series.DataPoint
import im.dacer.androidcharts.PieHelper
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DataGraph {
    companion object{
        fun getCovidInfection(): Array<DataPoint> {
            return arrayOf(
                DataPoint(getDateFromString("20.01.22"), 2795.0),
                DataPoint(getDateFromString("21.01.22"), 4424.0),
                DataPoint(getDateFromString("22.01.22"), 6400.0),
                DataPoint(getDateFromString("23.01.22"), 7056.0),
                DataPoint(getDateFromString("24.01.22"), 5908.0),
                DataPoint(getDateFromString("25.01.22"), 5769.0),
                DataPoint(getDateFromString("26.01.22"), 6709.0),
                DataPoint(getDateFromString("27.01.22"), 7182.0),
                DataPoint(getDateFromString("28.01.22"), 8611.0),
            )
        }

        fun getCovidDead(): Array<DataPoint> {
            return arrayOf(
                DataPoint(getDateFromString("20.01.22"), 39.0),
                DataPoint(getDateFromString("21.01.22"), 41.0),
                DataPoint(getDateFromString("22.01.22"), 38.0),
                DataPoint(getDateFromString("23.01.22"), 42.0),
                DataPoint(getDateFromString("24.01.22"), 39.0),
                DataPoint(getDateFromString("25.01.22"), 40.0),
                DataPoint(getDateFromString("26.01.22"), 31.0),
                DataPoint(getDateFromString("27.01.22"), 34.0),
                DataPoint(getDateFromString("28.01.22"), 38.0),
            )
        }

        fun getPopulation(): Pair<ArrayList<ArrayList<Int>>, ArrayList<String>>{
            return Pair(
                arrayListOf(
                    arrayListOf(
                        1391990, 4570836, 4744200, 5863003, 5774529, 6359385, 6581000,
                        6693623, 6700180, 6720248, 6719121, 6704332, 6677307, 6671785,
                        6660192, 6657710, 6657149, 6653499, 6628173, 6613469, 6618538,
                        6616879, 6622017, 6629703, 6628107, 6645672, 6672773, 6712582,
                        7095120, 7106215, 7198686, 7048084, 7133620, 7231068, 7318647,
                        7423470, 7503385, 7599647, 7690863, 7708499, 7765918
                    )),
                    arrayListOf(
                        "1897", "1926", "1928", "1959", "1970", "1979", "1987",
                        "1989", "1990", "1991", "1992", "1993", "1994", "1995",
                        "1996", "1997", "1998", "1999", "2000", "2001", "2002",
                        "2003", "2004", "2005", "2006", "2007", "2008", "2009",
                        "2010", "2011", "2012", "2013", "2014", "2015", "2016",
                        "2017", "2018", "2019", "2020", "2021", "2022"
                    )
            )
        }

        fun getNotion(): Pair<ArrayList<PieHelper>, ArrayList<String>>{
            return Pair(arrayListOf(
                PieHelper(87.42f,  Color.BLUE), //"Русские"
                PieHelper(1.68f,  Color.YELLOW), //"Украинцы"
                PieHelper(1.89f,  Color.GRAY), //"Армяне"
                PieHelper(0.79f,  Color.CYAN), //"Татары"
                PieHelper(0.45f,  Color.GREEN), //"Белорусы"
                PieHelper(7.77f, Color.BLACK)   //Другое
            ), arrayListOf(
                "Русские", "Украинцы", "Армяне", "Татары", "Белорусы", "Другое"
            ))
        }

        fun getCrime(): LineData {
            return LineData(
                listOf<LineDataSet>(
                    LineDataSet(arrayListOf(
                        Entry(2014f, 91061f),
                        Entry(2015f, 89183f),
                        Entry(2016f, 88297f),
                        Entry(2017f, 84307f),
                        Entry(2018f, 80299f),
                    ), "Всего преступлений"),
                    LineDataSet(arrayListOf(
                        Entry(2014f, 162f),
                        Entry(2015f, 158f),
                        Entry(2016f, 139f),
                        Entry(2017f, 131f),
                        Entry(2018f, 110f),
                    ), "Изнасилования"),
                    LineDataSet(arrayListOf(
                        Entry(2014f, 144f),
                        Entry(2015f, 153f),
                        Entry(2016f, 157f),
                        Entry(2017f, 155f),
                        Entry(2018f, 154f),
                    ), "Хулиганство"),
                    LineDataSet(arrayListOf(
                        Entry(2014f, 73f),
                        Entry(2015f, 60f),
                        Entry(2016f, 69f),
                        Entry(2017f, 60f),
                        Entry(2018f, 65f),
                    ), "Хищение оружия"),
                    LineDataSet(arrayListOf(
                        Entry(2014f, 3317f),
                        Entry(2015f, 2952f),
                        Entry(2016f, 2724f),
                        Entry(2017f, 2709f),
                        Entry(2018f, 2415f),
                    ), "Габежи"),
                )
            )
        }

        fun getFreeCrime(): PieData {
            return PieData(
                PieDataSet(
                    arrayListOf(
                        PieEntry(1328f, "грабежи"),
                        PieEntry(1062f, "мошенничество"),
                        PieEntry(100f, "вымогательство"),
                        PieEntry(9381f, "кражи"),
                    ), "Раскрываемость преступлений за 2018 год")
            )
        }

        fun getDateFromString(date: String): Date {
            return SimpleDateFormat("dd.MM.yy", Locale.getDefault()).parse(date)!!
        }

        fun convertLongToDateString(value: Long): String{
            return SimpleDateFormat("dd.MM.yy", Locale.getDefault()).format(Date(value))
        }
    }
}