package com.ege.graphapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ege.graphapplication.fragments.FragmentAndroidCharts
import com.ege.graphapplication.fragments.FragmentCovidGraphView
import com.ege.graphapplication.fragments.FragmentMPAndroidChart
import com.google.android.material.tabs.TabLayout
import com.jjoe64.graphview.LabelFormatter
import com.jjoe64.graphview.Viewport
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_graph_view.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pager.adapter = FragmentAdapterViewPager(this, arrayListOf(
            FragmentCovidGraphView(),
            FragmentAndroidCharts(),
            FragmentMPAndroidChart()
        ))
        pager.isUserInputEnabled = false
        CustomTabLayout(tabLayout).dotsToViewPager2(pager)
    }
}

