package com.ege.graphapplication.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.fragment.app.Fragment
import com.ege.graphapplication.DataGraph
import com.ege.graphapplication.R
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import kotlinx.android.synthetic.main.fragment_mpa_android_charts.*

class FragmentMPAndroidChart: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mpa_android_charts, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val data = DataGraph.getCrime()
        val colors = arrayListOf(
            Color.Blue,
            Color.Red,
            Color.LightGray,
            Color.Green,
            Color.Cyan,
            Color.DarkGray,
            Color.Yellow,
            Color.Magenta,
            Color.Gray,
        )
        data.dataSets.forEachIndexed() { index, it -> (it as LineDataSet)
            it.lineWidth = 2.5f
            it.color = colors[index].toArgb()
            it.setCircleColor(colors[index].toArgb())
        }
        data.setValueTextColor(Color.White.toArgb())
        lineChart.legend.textColor = Color.White.toArgb()
        lineChart.axisLeft.textColor = Color.White.toArgb()
        lineChart.axisRight.isEnabled = false
        lineChart.xAxis.textColor = Color.White.toArgb()
        lineChart.description.textColor = Color.White.toArgb()
        lineChart.description.text = "ЧИСЛО ЗАРЕГИСТРИРОВАННЫХ ПРЕСТУПЛЕНИЙ"
        lineChart.legend.isWordWrapEnabled = true
        lineChart.data = data
        lineChart.invalidate()

        val dataPie = DataGraph.getFreeCrime()
        dataPie.setValueTextSize(12f)
        (dataPie.dataSet as PieDataSet).colors = ColorTemplate.MATERIAL_COLORS.toMutableList()
        pieChart.data = dataPie
        pieChart.centerText = "Раскрываемость преступлений за 2018 год"
        pieChart.setCenterTextColor(Color.White.toArgb())
        pieChart.setHoleColor(Color.Black.toArgb())
        pieChart.setDrawEntryLabels(false)
        pieChart.holeRadius = 75f
        pieChart.legend.form = Legend.LegendForm.CIRCLE
        pieChart.legend.textColor = Color.White.toArgb()
        pieChart.legend.isWordWrapEnabled = true
        pieChart.invalidate()
    }


}