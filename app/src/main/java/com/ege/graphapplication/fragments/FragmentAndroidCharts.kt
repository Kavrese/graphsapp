package com.ege.graphapplication.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ege.graphapplication.DataGraph
import com.ege.graphapplication.R
import com.google.android.material.snackbar.Snackbar
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.LabelFormatter
import com.jjoe64.graphview.Viewport
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import im.dacer.androidcharts.LineView
import kotlinx.android.synthetic.main.fragment_android_charts.*
import kotlinx.android.synthetic.main.fragment_graph_view.*

class FragmentAndroidCharts(): Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_android_charts, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val data = DataGraph.getPopulation()
        val dataList = data.first
        val strList = data.second
        line_view.setBottomTextList(strList)
        line_view.setDataList(dataList)
        line_view.setDrawDotLine(true)
        line_view.setShowPopup(LineView.SHOW_POPUPS_MAXMIN_ONLY);

        val dataPie = DataGraph.getNotion()
        pie_view.setDate(dataPie.first)
        pie_view.showPercentLabel(true)
        pie_view.setOnPieClickListener {
            if (it >= 0 && it < dataPie.second.size)
                Snackbar.make(pie_view, "${dataPie.second[it]} - ${"%.2f".format(dataPie.first[it].sweep * 100 / 360)}", Snackbar.LENGTH_SHORT).show()
        }
    }
}