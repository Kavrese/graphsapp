package com.ege.graphapplication.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ege.graphapplication.DataGraph
import com.ege.graphapplication.R
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.LabelFormatter
import com.jjoe64.graphview.Viewport
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.fragment_graph_view.*

class FragmentCovidGraphView(): Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_graph_view, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        graph.title = "Заражение COVID-19 по Московсокй области"
        graph_2.title = "Летальный исход COVID-19 по Московсокй области"

        val seriesInfection = BarGraphSeries<DataPoint>(DataGraph.getCovidInfection())
        seriesInfection.color = Color.rgb(252, 15, 3)
        seriesInfection.isAnimated = true

        val seriesDead = LineGraphSeries<DataPoint>(DataGraph.getCovidDead())
        seriesDead.color = Color.rgb(3, 127, 252)
        seriesDead.setAnimated(true)
        seriesDead.isDrawDataPoints = true
        seriesDead.dataPointsRadius = 10f

        graph.addSeries(seriesInfection)
        graph_2.addSeries(seriesDead)

        prepareGraph(graph)
        prepareGraph(graph_2)
    }

    private fun prepareGraph(graphView: GraphView){
        graphView.viewport.isScalable = true
        graphView.gridLabelRenderer.setHorizontalLabelsAngle(45)
        graphView.gridLabelRenderer.labelFormatter = object: LabelFormatter{
            override fun formatLabel(value: Double, isValueX: Boolean): String {
                return if (isValueX)
                    DataGraph.convertLongToDateString(value.toLong())
                else value.toString()
            }
            override fun setViewport(viewport: Viewport?) {}
        }
    }
}