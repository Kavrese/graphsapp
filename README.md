# GraphApplication
### Приложение-тестирование библиотек для графиков
Приложение реализовывает 6 графиков различных видов с использованием соотвествующих библиотек.
#### Перечень используемых библиотек
- [GraphView](https://github.com/jjoe64/GraphView)
- [AndroidCharts](https://github.com/HackPlan/AndroidCharts)
- [MPAndroidChart](https://github.com/PhilJay/MPAndroidChart)
## Скриншоты
<p>
  <img src="https://gitlab.com/Kavrese/graphsapp/raw/master/photos/Screenshot_2022_0202_234706.jpg" width="200" />
  <img src="https://gitlab.com/Kavrese/graphsapp/raw/master/photos/Screenshot_2022_0202_234721.jpg" width="200" /> 
  <img src="https://gitlab.com/Kavrese/graphsapp/raw/master/photos/Screenshot_2022_0202_234754.jpg" width="200" />
</p>

## Дополнительная информация
Все данные для статистики были взяты из [МОСКОВСКАЯ ОБЛАСТЬ В ЦИФРАХ СТАТИСТИЧЕСКИЙ СБОРНИК](https://mosstat.gks.ru/storage/mediabank/XKtXCDM1/%D0%9C%D0%9E%20%D0%B2%20%D1%86%D0%B8%D1%84%D1%80%D0%B0%D1%85%202018.pdf), собранный в 2019 году ФЕДЕРАЛЬНОЙ СЛУЖБОЙ ГОСУДАРСТВЕННОЙ СТАТИСТИКИ УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ СЛУЖБЫ ГОСУДАРСТВЕННОЙ СТАТИСТИКИ ПО Г. МОСКВЕ И МОСКОВСКОЙ ОБЛАСТИ
